import pygame, sys, time, random
from pygame.locals import *

# 窗口
WINDOW_WIDTH  = 640
WINDOW_HEIGHT = 480


GAME_STATE_INIT        = 0
GAME_STATE_START_LEVEL = 1
GAME_STATE_RUN         = 2
GAME_STATE_GAMEOVER    = 3
GAME_STATE_SHUTDOWN    = 4
GAME_STATE_EXIT        = 5

# 小球
BALL_START_Y  = (WINDOW_HEIGHT-150)
BALL_SIZE     =5
T = 0
precense = 0
chance_show = 0
show=0
# 擋版
PADDLE_START_X  = (WINDOW_WIDTH/2 - 16)
PADDLE_START_Y  = (WINDOW_HEIGHT -10);
PADDLE_WIDTH    = 150
PADDLE_HEIGHT   = 8
PADDLE2_START_X  = (WINDOW_WIDTH/2 - 16)
PADDLE2_START_Y  = (WINDOW_HEIGHT - 480);
PADDLE3_START_X  = (WINDOW_WIDTH - 640)
PADDLE3_START_Y  = (WINDOW_HEIGHT - 300);
PADDLE4_START_X  = (WINDOW_WIDTH - 10)
PADDLE4_START_Y  = (WINDOW_HEIGHT - 300);
CHANCE = 0.0
# 磚塊
NUM_BLOCK_ROWS    = 6
NUM_BLOCK_COLUMNS = 8
BLOCK_WIDTH       = 50
BLOCK_HEIGHT      = 16
BLOCK_ORIGIN_X    = 10
BLOCK_ORIGIN_Y    = 130
BLOCK_X_GAP       = 80
BLOCK_Y_GAP       = 32


# 颜色
BACKGROUND_COLOR = (0, 0, 0)
BALL_COLOR       = (0, 0, 255)
BALL1_COLOR       = (255, 0, 0)
PADDLE_COLOR     = (128, 64, 64)
BLOCK_COLOR      = (255, 128, 0)
BLOCK1_COLOR      = (255, 0, 0)
TEXT_COLOR       = (255, 255, 255)

# 属性信息
TOTAL_LIFE       = 5
FPS              = 25
screen = pygame.display.set_mode((600,500))
# 初始化磚塊
def InitBlocks():
    #blocks = [[1] * NUM_BLOCK_COLUMNS] * NUM_BLOCK_ROWS
    blocks = []
    for i in range(NUM_BLOCK_ROWS):             #@UnusedVarialbe
        blocks.append([i+1] * NUM_BLOCK_COLUMNS)
    return blocks


# 顯示文字
def DrawText(text, font, surface, x, y):
    text_obj = font.render(text, 1, TEXT_COLOR)
    text_rect = text_obj.get_rect()
    text_rect.topleft = (x, y)
    surface.blit(text_obj, text_rect)

# 退出
def Terminate():
    pygame.quit()
    sys.exit()

# 等待用戶輸入
def WaitForPlayerToPressKey():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                Terminate()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    Terminate()
                return

# 游戲界面的初始化
pygame.init()
mainClock = pygame.time.Clock()

# 小球的位置和速度
ball_x  = 20
ball_y  = 20
ball_dx = 0
ball_dy = 0

ball_x1  = 20
ball_y1  = 20
ball_dx1 = 0
ball_dy1 = 0

ball_x2  = 20
ball_y2  = 20
ball_dx2 = 0
ball_dy2 = 0

# 擋板運動控制
paddle_move_left  = False
paddle_move_right = False
paddle_move_up  = False
paddle_move_down = False

# 擋板的位置與顏射
paddle  = {'rect' :pygame.Rect(0, 0, PADDLE_WIDTH, PADDLE_HEIGHT),
           'color': PADDLE_COLOR}
paddle2  = {'rect' :pygame.Rect(0, 0, PADDLE_WIDTH, PADDLE_HEIGHT),
           'color': PADDLE_COLOR}
paddle3  = {'rect' :pygame.Rect(0, 0, PADDLE_HEIGHT, PADDLE_WIDTH),
           'color': PADDLE_COLOR}
paddle4  = {'rect' :pygame.Rect(0, 0, PADDLE_HEIGHT,PADDLE_WIDTH),
           'color': PADDLE_COLOR}
# 遊戲狀態
game_state  = GAME_STATE_INIT
blocks      = []
life_left   = TOTAL_LIFE
game_over   = False
blocks_hit  = 0
score       = 0
level       = 1

game_start_font = pygame.font.SysFont(None, 48)
game_over_font  = pygame.font.SysFont(None, 48)
text_font       = pygame.font.SysFont(None, 20)



windowSurface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
pygame.display.set_caption('打磚塊')


DrawText('pyFreakOut', game_start_font, windowSurface,
         (WINDOW_WIDTH/3), (WINDOW_HEIGHT/3 + 50))
DrawText('Press any key to start.', game_start_font, windowSurface,
         (WINDOW_WIDTH/3)-60, (WINDOW_HEIGHT)/3+100)
pygame.display.update()
WaitForPlayerToPressKey()




# 主循環
while True:
    # 事件
    for event in pygame.event.get():
        if event.type == QUIT:
            Terminate()
        if event.type == KEYDOWN:
            if event.key == K_a:
                paddle_move_left = True
            if event.key == K_d:
                paddle_move_right = True
            if event.key == K_w:
                paddle_move_up = True
            if event.key == K_s:
                paddle_move_down = True
            if event.key == K_ESCAPE:
                Terminate()
        if event.type == KEYUP:
            if event.key == K_a:
                paddle_move_left = False
            if event.key == K_d:
                paddle_move_right = False
            if event.key == K_w:
                paddle_move_up = False
            if event.key == K_s:
                paddle_move_down = False

    # 遊戲控制流程
    if game_state == GAME_STATE_INIT:
        # 初始化遊戲
        ball_x  = random.randint(8, WINDOW_WIDTH-8)
        ball_y  = BALL_START_Y
        ball_dx = random.randint(-10, 4)
        ball_dy = random.randint( 2, 8)
        ball_x1  = random.randint(20, WINDOW_WIDTH-8)
        ball_y1  = BALL_START_Y
        ball_dx1 = random.randint(5, 10)
        ball_dy1 = random.randint( 2, 8)
        ball_x2  = random.randint(10, WINDOW_WIDTH-8)
        ball_y2  = BALL_START_Y
        ball_dx2 = random.randint(6, 9)
        ball_dy2 = random.randint( 2, 8)


        paddle['rect'].left = PADDLE_START_X
        paddle['rect'].top  = PADDLE_START_Y
        paddle2['rect'].left = PADDLE2_START_X
        paddle2['rect'].top  = PADDLE2_START_Y
        paddle3['rect'].left = PADDLE3_START_X
        paddle3['rect'].top  = PADDLE3_START_Y
        paddle4['rect'].left = PADDLE4_START_X
        paddle4['rect'].top  = PADDLE4_START_Y

        paddle_move_left  = False
        paddle_move_right = False
        paddle_move_up  = False
        paddle_move_down = False


        life_left   = TOTAL_LIFE
        game_over   = False
        blocks_hit  = 0
        score       = 0
        level       = 1
        game_state  = GAME_STATE_START_LEVEL
    elif game_state == GAME_STATE_START_LEVEL:
        # 新的一關
        blocks = InitBlocks()
        game_state = GAME_STATE_RUN
    elif game_state == GAME_STATE_RUN:
        # 游戲運行

        # 球的運動
        ball_x += ball_dx;
        ball_y += ball_dy;
        ball_x1 += ball_dx1;
        ball_y1 += ball_dy1;
        ball_x2 += ball_dx2;
        ball_y2 += ball_dy2;



        if ball_x > (WINDOW_WIDTH-BALL_SIZE) or ball_x < BALL_SIZE:
            if life_left == 0:
                game_state = GAME_STATE_GAMEOVER
            else:
                #扣命
                life_left -= 1
                # 初始化游戲
                ball_x  = paddle2['rect'].left + PADDLE_WIDTH // 2
                ball_y  = BALL_START_Y
                ball_dx = random.randint(-4, 5)
                ball_dy = random.randint( 6, 9)
        elif ball_y < BALL_SIZE:
            if life_left == 0:
                game_state = GAME_STATE_GAMEOVER
            else:
                #扣命
                life_left -= 1
                # 初始化游戲
                ball_x  = paddle2['rect'].left + PADDLE_WIDTH // 2
                ball_y  = BALL_START_Y
                ball_dx = random.randint(-4, 5)
                ball_dy = random.randint( 6, 9)
        elif ball_y > WINDOW_HEIGHT-BALL_SIZE:
            if life_left == 0:
                game_state = GAME_STATE_GAMEOVER
            else:
                #扣命
                life_left -= 1
                # 初始化游戲
                ball_x  = paddle['rect'].left + PADDLE_WIDTH // 2
                ball_y  = BALL_START_Y
                ball_dx = random.randint(-4, 5)
                ball_dy = random.randint( 6, 9)

        # 檢测球是否與擋板碰撞
        if ball_y > WINDOW_HEIGHT // 2: #down
            if (ball_x+BALL_SIZE >= paddle['rect'].left and \
                ball_x-BALL_SIZE <= paddle['rect'].left+PADDLE_WIDTH and \
                ball_y+BALL_SIZE >= paddle['rect'].top and \
                ball_y-BALL_SIZE <= paddle['rect'].top+PADDLE_HEIGHT):
                ball_dy = - ball_dy
                ball_y += ball_dy
                if paddle_move_left:
                    ball_dx -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx += random.randint(0, 3)
                else:
                    ball_dx += random.randint(-1, 2)
        if ball_y < WINDOW_HEIGHT-480 // 2: #up
            if (ball_x+BALL_SIZE >= paddle2['rect'].left and \
                ball_x-BALL_SIZE <= paddle2['rect'].left+PADDLE_WIDTH and \
                ball_y+BALL_SIZE >= paddle2['rect'].top and \
                ball_y-BALL_SIZE <= paddle2['rect'].top+PADDLE_HEIGHT):
                ball_dy = - ball_dy
                ball_y += ball_dy
                if paddle_move_left:
                    ball_dx -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx += random.randint(0, 3)
                else:
                    ball_dx += random.randint(-1, 2)

        if (True): #left
            if (ball_y+BALL_SIZE <= paddle3['rect'].top+PADDLE_WIDTH and \
                ball_y-BALL_SIZE >= paddle3['rect'].top and \
                ball_x-BALL_SIZE <= paddle3['rect'].left+PADDLE_HEIGHT and \
                ball_x+BALL_SIZE >= paddle3['rect'].left):
                CHANCE = 1
                if(True):
                    ball_dx *= (-1)
                    ball_x += ball_dx
                    if paddle_move_up:
                        ball_dx += random.randint(0, 3)
                    elif paddle_move_down:
                        ball_dx -= random.randint(0, 3)
                    else:
                        ball_dx += random.randint(-1, 2)

        if (True):   #right
            if(ball_y+BALL_SIZE <= paddle4['rect'].top+PADDLE_WIDTH and \
                ball_y-BALL_SIZE >= paddle4['rect'].top and \
                ball_x-BALL_SIZE <= paddle4['rect'].left+PADDLE_HEIGHT and \
                ball_x+BALL_SIZE >= paddle4['rect'].left):
                CHANCE = 1
                if(True):
                    ball_dx *= (-1)
                    ball_x += ball_dx
                    if paddle_move_up:
                        ball_dx += random.randint(0, 3)
                    elif paddle_move_down:
                        ball_dx -= random.randint(0, 3)
                    else:
                        ball_dx += random.randint(-1, 2)



        if ball_y1 > WINDOW_HEIGHT // 2: #down
            if (ball_x1+BALL_SIZE >= paddle['rect'].left and \
                ball_x1-BALL_SIZE <= paddle['rect'].left+PADDLE_WIDTH and \
                ball_y1+BALL_SIZE >= paddle['rect'].top and \
                ball_y1-BALL_SIZE <= paddle['rect'].top+PADDLE_HEIGHT):
                ball_dy1 = - ball_dy1
                ball_y1 += ball_dy1
                if paddle_move_left:
                    ball_dx1 -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx1 += random.randint(0, 3)
                else:
                    ball_dx1 += random.randint(-1, 2)
        if ball_y1 < WINDOW_HEIGHT-480 // 2: #up
            if (ball_x1+BALL_SIZE >= paddle2['rect'].left and \
                ball_x1-BALL_SIZE <= paddle2['rect'].left+PADDLE_WIDTH and \
                ball_y1+BALL_SIZE >= paddle2['rect'].top and \
                ball_y1-BALL_SIZE <= paddle2['rect'].top+PADDLE_HEIGHT):
                ball_dy1 = - ball_dy1
                ball_y1 += ball_dy1
                if paddle_move_left:
                    ball_dx1 -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx1 += random.randint(0, 3)
                else:
                    ball_dx1 += random.randint(-1, 2)

        if (True): #left
            if (ball_y1+BALL_SIZE <= paddle3['rect'].top+PADDLE_WIDTH and \
                ball_y1-BALL_SIZE >= paddle3['rect'].top and \
                ball_x1-BALL_SIZE <= paddle3['rect'].left+PADDLE_HEIGHT and \
                ball_x1+BALL_SIZE >= paddle3['rect'].left):
                CHANCE = 1
                if(True):
                    ball_dx1 *= (-1)
                    ball_x1 += ball_dx1
                    if paddle_move_up:
                        ball_dx1 += random.randint(0, 3)
                    elif paddle_move_down:
                        ball_dx1 -= random.randint(0, 3)
                    else:
                        ball_dx1 += random.randint(-1, 2)

        if (True):   #right
            if(ball_y1+BALL_SIZE <= paddle4['rect'].top+PADDLE_WIDTH and \
                ball_y1-BALL_SIZE >= paddle4['rect'].top and \
                ball_x1-BALL_SIZE <= paddle4['rect'].left+PADDLE_HEIGHT and \
                ball_x1+BALL_SIZE >= paddle4['rect'].left):
                CHANCE = 1
                if(True):
                    ball_dx1 *= (-1)
                    ball_x1 += ball_dx1
                    if paddle_move_up:
                        ball_dx1 += random.randint(0, 3)
                    elif paddle_move_down:
                        ball_dx1 -= random.randint(0, 3)
                    else:
                        ball_dx1 += random.randint(-1, 2)
        if ball_y2 > WINDOW_HEIGHT // 2: #down
            if (ball_x2+BALL_SIZE >= paddle['rect'].left and \
                ball_x2-BALL_SIZE <= paddle['rect'].left+PADDLE_WIDTH and \
                ball_y2+BALL_SIZE >= paddle['rect'].top and \
                ball_y2-BALL_SIZE <= paddle['rect'].top+PADDLE_HEIGHT):
                ball_dy2 = - ball_dy2
                ball_y2 += ball_dy2
                if paddle_move_left:
                    ball_dx2 -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx2 += random.randint(0, 3)
                else:
                    ball_dx2 += random.randint(-1, 2)
        if ball_y2 < WINDOW_HEIGHT-480 // 2: #up
            if (ball_x2+BALL_SIZE >= paddle2['rect'].left and \
                ball_x2-BALL_SIZE <= paddle2['rect'].left+PADDLE_WIDTH and \
                ball_y2+BALL_SIZE >= paddle2['rect'].top and \
                ball_y2-BALL_SIZE <= paddle2['rect'].top+PADDLE_HEIGHT):
                ball_dy2 = - ball_dy2
                ball_y2 += ball_dy2
                if paddle_move_left:
                    ball_dx2 -= random.randint(0, 3)
                elif paddle_move_right:
                    ball_dx2 += random.randint(0, 3)
                else:
                    ball_dx2 += random.randint(-1, 2)

        if (True): #left
            if (ball_y2+BALL_SIZE <= paddle3['rect'].top+PADDLE_WIDTH and \
                ball_y2-BALL_SIZE >= paddle3['rect'].top and \
                ball_x2-BALL_SIZE <= paddle3['rect'].left+PADDLE_HEIGHT and \
                ball_x2+BALL_SIZE >= paddle3['rect'].left):
                ball_dx2 *= (-1)
                ball_x2 += ball_dx2
                if paddle_move_up:
                    ball_dx2 += random.randint(0, 3)
                elif paddle_move_down:
                    ball_dx2 -= random.randint(0, 3)
                else:
                    ball_dx2 += random.randint(-1, 2)

        if (True):   #right
            if(ball_y2+BALL_SIZE <= paddle4['rect'].top+PADDLE_WIDTH and \
                ball_y2-BALL_SIZE >= paddle4['rect'].top and \
                ball_x2-BALL_SIZE <= paddle4['rect'].left+PADDLE_HEIGHT and \
                ball_x2+BALL_SIZE >= paddle4['rect'].left):
                ball_dx2 *= (-1)
                ball_x2 += ball_dx2
                if paddle_move_up:
                    ball_dx2 += random.randint(0, 3)
                elif paddle_move_down:
                    ball_dx2 -= random.randint(0, 3)
                else:
                    ball_dx2 += random.randint(-1, 2)
        if(ball_x1 < 0 or ball_x2 < 0 or ball_y1 < 0 or ball_y2 < 0) :
            if show >0:
                show -= 1
        if(ball_x1 > WINDOW_WIDTH or ball_x2 > WINDOW_WIDTH or ball_y1 > WINDOW_HEIGHT or ball_y2 > WINDOW_HEIGHT) :
            if show >0:
                show -= 1

        # 檢测球是否與磚塊碰撞
        cur_x = BLOCK_ORIGIN_X
        cur_y = BLOCK_ORIGIN_Y
        for row in range(NUM_BLOCK_ROWS):
            cur_x = BLOCK_ORIGIN_X

            for col in range(NUM_BLOCK_COLUMNS):
                if blocks[row][col] != 0:

                    if (ball_x+BALL_SIZE >= cur_x and \
                        ball_x-BALL_SIZE <= cur_x+BLOCK_WIDTH and \
                        ball_y+BALL_SIZE >= cur_y and \
                        ball_y-BALL_SIZE <= cur_y+BLOCK_HEIGHT):
                        blocks[row][col] = 0
                        if(show == 0) :
                            show=2
                            ball_x1 = ball_x2 = ball_x
                            ball_y1 = ball_y2 = ball_y

                        blocks_hit += 1
                        ball_dy = -ball_dy
                        ball_dx += random.randint(-1, 2)
                        score += 5 * (level + abs(ball_dx))
                        if (random.random() < 1):
                            T = 50
                            BALL_SIZE = 20
                    if (ball_x1+BALL_SIZE >= cur_x and \
                        ball_x1-BALL_SIZE <= cur_x+BLOCK_WIDTH and \
                        ball_y1+BALL_SIZE >= cur_y and \
                        ball_y1-BALL_SIZE <= cur_y+BLOCK_HEIGHT):
                        blocks[row][col] = 0
                        blocks_hit += 1
                        ball_dy1 = -ball_dy1
                        ball_dx1 += random.randint(-1, 2)
                        score += 5 * (level + abs(ball_dx1))
                    if (ball_x2+BALL_SIZE >= cur_x and \
                        ball_x2-BALL_SIZE <= cur_x+BLOCK_WIDTH and \
                        ball_y2+BALL_SIZE >= cur_y and \
                        ball_y2-BALL_SIZE <= cur_y+BLOCK_HEIGHT):
                        blocks[row][col] = 0
                        blocks_hit += 1
                        ball_dy2 = -ball_dy2
                        ball_dx2 += random.randint(-1, 2)
                        score += 5 * (level + abs(ball_dx2))
                cur_x += BLOCK_X_GAP
            cur_y += BLOCK_Y_GAP

        if (T == 0) :
            BALL_SIZE = 5
        elif(T != 0) :
            T -= 1
        print(ball_x1)



        if blocks_hit == NUM_BLOCK_ROWS * NUM_BLOCK_COLUMNS:
            level       += 1
            blocks_hit  = 0
            score       += 1000
            show = 0
            game_state  = GAME_STATE_START_LEVEL



        # 擋板的運動
        if paddle_move_left:
            paddle['rect'].left -= 24
            if paddle['rect'].left < 0:
                paddle['rect'].left = 0
        if paddle_move_right:
            paddle['rect'].left += 24
            if paddle['rect'].left > WINDOW_WIDTH-PADDLE_WIDTH:
                paddle['rect'].left = WINDOW_WIDTH-PADDLE_WIDTH
        if paddle_move_left:
            paddle2['rect'].left -= 24
            if paddle2['rect'].left < 0:
                paddle2['rect'].left = 0
        if paddle_move_right:
            paddle2['rect'].left += 24
            if paddle2['rect'].left > WINDOW_WIDTH-PADDLE_WIDTH:
                paddle2['rect'].left = WINDOW_WIDTH-PADDLE_WIDTH
        if paddle_move_up:
            paddle3['rect'].top -= 24
            if paddle3['rect'].top < 0:
                paddle3['rect'].top = 0
        if paddle_move_down:
            paddle3['rect'].top += 24
            if paddle3['rect'].top > WINDOW_HEIGHT-PADDLE_HEIGHT:
                paddle3['rect'].top = WINDOW_HEIGHT-PADDLE_HEIGHT
        if paddle_move_up:
            paddle4['rect'].top -= 24
            if paddle4['rect'].top < 0:
                paddle4['rect'].top = 0
        if paddle_move_down:
            paddle4['rect'].top += 24
            if paddle4['rect'].top > WINDOW_HEIGHT-PADDLE_HEIGHT:
                paddle4['rect'].top = WINDOW_HEIGHT-PADDLE_HEIGHT
        # 繪製
        windowSurface.fill(BACKGROUND_COLOR)
        # 繪製擋板
        pygame.draw.rect(windowSurface, paddle['color'], paddle['rect'])
        pygame.draw.rect(windowSurface, paddle2['color'], paddle2['rect'])
        pygame.draw.rect(windowSurface, paddle3['color'], paddle3['rect'])
        pygame.draw.rect(windowSurface, paddle4['color'], paddle4['rect'])
        # 繪製小球
        pygame.draw.circle(windowSurface, BALL_COLOR, (ball_x, ball_y),
                           BALL_SIZE, 0)
        if(show >0):
            pygame.draw.circle(windowSurface, (0,255,0), (ball_x1, ball_y1),BALL_SIZE, 0)
            pygame.draw.circle(windowSurface, (255,0,0), (ball_x2, ball_y2),BALL_SIZE, 0)
        else:
            ball_x1 = ball_x2 = ball_y1 = ball_y2 = 0


        # 繪製磚塊
        cur_x = BLOCK_ORIGIN_X
        cur_y = BLOCK_ORIGIN_Y
        for row in range(NUM_BLOCK_ROWS):
            cur_x = BLOCK_ORIGIN_X
            for col in range(NUM_BLOCK_COLUMNS):
                if blocks[row][col] != 0:
                    pygame.draw.rect(windowSurface, BLOCK_COLOR,
                                     (cur_x, cur_y, BLOCK_WIDTH, BLOCK_HEIGHT))
                cur_x += BLOCK_X_GAP
            cur_y += BLOCK_Y_GAP




        # 繪製文字描述信息
        message = 'Level: ' + str(level) + '    Life: ' + str(life_left) + '    Score: ' + str(score)
        DrawText(message, text_font, windowSurface, 8, (WINDOW_HEIGHT - 16))
    elif game_state == GAME_STATE_GAMEOVER:
        DrawText('GAME OVER', game_over_font, windowSurface,
                 (WINDOW_WIDTH / 3), (WINDOW_HEIGHT / 3))
        DrawText('Level: ' + str(level), game_over_font, windowSurface,
                 (WINDOW_WIDTH / 3)+20, (WINDOW_HEIGHT / 3) + 50)
        DrawText('Score: ' + str(score), game_over_font, windowSurface,
                 (WINDOW_WIDTH / 3)+20, (WINDOW_HEIGHT / 3) + 100)
        DrawText('Press any key to play again.', game_over_font, windowSurface,
                 (WINDOW_WIDTH / 3)-80, (WINDOW_HEIGHT / 3) + 150)
        pygame.display.update()

        pygame.mixer.music.stop()

        show = 0


        WaitForPlayerToPressKey()
        game_state = GAME_STATE_INIT
    elif game_state == GAME_STATE_SHUTDOWN:
        game_state = GAME_STATE_EXIT

    pygame.display.update()
    mainClock.tick(FPS + level*2)
