import os
import math


pre_order = []
post_order = []


class Node:

    def __init__(self, value):  # 一個單獨節點的class
        self.left = None
        self.right = None
        self.val = value

    def __repr__(self):  # 當print Node 的時候可以回傳 value
        return str(self.val)

    def __eq__(self, compare):  # 判斷 Node a = Node b ?
        return self.val == compare.value()

    def insert(self, value):  # 一個ˊNode的時候插入左右邊
        if self.val:
            if value < self.val:
                if self.left is None:
                    self.left = Node(value)
                    return self.left
                else:
                    return self.left.insert(value)
            elif value > self.val:
                if self.right is None:
                    self.right = Node(value)
                    return self.right
                else:
                    return self.right.insert(value)
        else:
            self.val = value

    def find(self, value):  # 尋找節點
        if value < self.val:
            if self.left is None:
                return None
            return self.left.find(value)
        elif value > self.val:
            if self.right is None:
                return None
            return self.right.find(value)
        else:
            return self

    def minimum(n):  # 找最小的 Node
        current = n
        while current.left is not None:
            current = current.left
        return current

    def deleteNode(self, value):  # 刪除 Node
        if value < self.val:
            if self.left is not None:
                self.left = self.left.deleteNode(value)
            else:
                print('Key', value, 'is not found')
        elif value > self.val:
            if self.right is not None:
                self.right = self.right.deleteNode(value)
            else:
                print('Key', value, 'is not found')
        else:
            if self.left is None:
                return self.right
            elif self.right is None:
                return self.left
            temp = minimum(self.right)
            self.val = temp.val
            self.right = self.right.deleteNode(temp.val)
        return self

    def left(self):  # 回傳左邊
        return self.left

    def right(self):  # 回傳右邊
        return self.right

    def height(self):  # 節點高度
        l = r = 0
        if self.left is not None:
            l = self.left.height() + 1
        if self.right is not None:
            r = self.right.height() + 1
        if l == 0 and r == 0:
            return 0
        return max(l, r)

    def preorder(self):
        global pre_order
        pre_order.append(self)
        if self.left is not None:
            self.left.preorder()
        if self.right is not None:
            self.right.preorder()
        return pre_order

    def postorder(self):
        global post_order
        if self.left is not None:
            self.left.postorder()
        if self.right is not None:
            self.right.postorder()
        post_order.append(self)

        return post_order

    def value(self):
        return self.val


class BinarySearchTree:  # 主要用來存取跟節點
    root = None

    def __init__(self):
        global root  # 要用到外面數值
        self.nodes = []
        root = None

    def __repr__(self):  # Print tree 得時候會回傳跟節點為參考值
        return root

    def searchkey(self, value):  # 利用 Node 的 Class 找節點
        if len(self.nodes) is 0:
            print('Not found')
        else:
            if self.nodes[0].find(value) is None:
                print('Not found')
            else:
                print(value, 'is found')

    def delete(self, value):  # 刪除節點 ( 以根結為參考點 )
        global root
        if self.nodes:
            root = root.deleteNode(value)
        else:
            print('Key', value, 'is not found')

    def insert(self, value):  # 加入節點
        global root
        if len(self.nodes) is 0:
            root = Node(value)
            self.nodes.append(root)
        else:
            self.nodes.append(root.insert(value))

    def print(self):  # T.print
        print(root.height())

    def list_pre(self):
        array = root.preorder()
        for i in array:
            print(i, end=', ')
        print('')

    def list_post(self):
        array = root.postorder()
        for i in array:
            print(i, end=', ')
        print('')


try:
    None
    # >----------------------------<
    #           Test
    # T = BinarySearchTree()
    # T.delete(5)
    # T.searchkey(9)
    # T.insert(35)
    # T.insert(45)
    # T.insert(40)
    # T.insert(43)
    # T.insert(41)
    # # T.insert(30)
    # T.insert(42)
    # T.print()
    # T.list_pre()
    # T.list_post()
    # >----------------------------<

except EOFError as error:
    print(error)
