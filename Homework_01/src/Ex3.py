import os


class fraction:
    def __init__(self, numerator, denominator):
        if denominator == 0:
            raise EOFError('Denominator connot be zero')
        self.get_num = numerator
        self.get_den = denominator
# 建立class儲存資料


def str_to_frac(str):
    str = str.strip()  # 減少輸入錯誤空格
    for i in range(len(str)):
        if(str[i] == '/'):
            up = int(str[0:i])  # 分子
            down = int(str[i + 1: len(str)])  # 分母
            return fraction(up, down)
            break
        elif i == (len(str) - 1) and (str[i] != '/'):
            raise EOFError('Input Error !!!')


try:
    user_input = str_to_frac(input('please input a fraction : '))
    print('The denominator is : ', user_input.get_den)  # 輸出分母
    print('The numerator is : ', user_input.get_num)  # 輸出分子
except EOFError as error:
    print('Error : ', error)  # 錯誤判斷
os.system('pause')
