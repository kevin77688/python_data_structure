import os


def is_bigger(a, b):
    print(max(a, b))  # 利用內建函式褲來判斷大小


try:
    print('Type bigger(a,b) to determine which value is bigger : ')
    s = input()

    first_order = 0     # 找左括號
    second_order = 0    # 找逗號
    end_order = 0       # 找右括號
    first_num = 0       # 第一個數字位數
    second_num = 0      # 第二個數字位數

    for index in range(len(str(s))):
        if s[index] == '(':
            first_order = index
        elif s[index] == ',':
            second_order = index
        elif s[index] == ')':
            end_order = index
    if first_order == 0 or second_order == 0 or end_order == 0:
        raise EOFError('Input error')

    #------------------------------------------------------------------------#
    # first_num_len = (second_order - first_order - 1)  # 第一個數字長度
    # second_num_len = (end_order - second_order - 1)  # 第二個數字長度
    #
    # for i in range(first_order + 1, second_order, 1):  # 建立第一個數字
    #     first_num = 10 * first_num + (ord(s[i]) - 48)
    #
    # for j in range(second_order + 1, end_order, 1):  # 建立第二個數字
    #     second_num = 10 * second_num + (ord(s[j]) - 48)
    #------------------------------------------------------------------------#

    first_num = float(s[first_order + 1: second_order])  # 第一個數字
    second_num = float(s[second_order + 1: end_order])  # 第二個數字

    is_bigger(first_num, second_num)
except EOFError as error:
    print(error)
os.system('pause')
