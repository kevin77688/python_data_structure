import os


def find_number(str):
    start = 0  # 找初始
    end = 0  # 找結束
    str = str.strip()
    for i in range(len(str)):
        if str[i] == '(':
            start = i + 1
        elif str[i] == ')':
            end = i
    return float(str[start:end])


def square_root(number):
    if number <= 0:
        raise EOFError('Input number cannot below zero')
    initial = number
    old_guess = number * 0.5
    for i in range(20):
        old_guess = 0.5 * (old_guess + (initial / old_guess))
    return old_guess


try:
    print('Enter square_root(number) to find sqrt() : ')
    number = find_number(input())
    print(square_root(number))
except EOFError as error:
    print('Error : ', error)
os.system('pause')
