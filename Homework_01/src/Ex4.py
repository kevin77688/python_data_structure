import os


def seletion_sort(_list):  # Selection sort 排序
    for i in range(len(_list)):
        min_index = i
        for j in range(i + 1, len(_list)):
            if float(_list[j]) < float(_list[min_index]):
                min_index = j
        if min_index != i:
            swap(_list, min_index, i)
    return _list


def swap(list, a, b):  # 順序交換
    list[a], list[b] = list[b], list[a]


print('Please enter the numbers to be sorted and separate them by spaces: ')
Ans = seletion_sort(input().split())  # 分別存入
print('The sorted array ', Ans)  # 輸出ANS

os.system('pause')
