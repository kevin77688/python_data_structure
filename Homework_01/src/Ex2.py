import sys
import os


def checkLength(str):
    if len(str) != 10:
        raise EOFError('Length error')  # 檢查ID長度


def changeID(str):
    IDchange = ['10', '11', '12', '13', '14', '15', '16', '17', '34',
                '18', '19', '20', '21', '22', '35', '23', '24', '25',
                '26', '27', '28', '29', '32', '30', '31', '33']
    # 建立A - Z 的轉換表格
    newstring = IDchange[ord(str[0]) - 65] + str[1:10]
    # 英文變成數字

    return newstring


def checkID(str):
    id_Multiple = [1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1]
    # 驗證倍率
    check_num = 0
    for i in range(11):
        if (ord(str[i]) - 48) < 10 and (ord(str[i]) - 48) >= 0:
            check_num += (ord(str[i]) - 48) * id_Multiple[i]
        else:
            raise EOFError('Input Error')
    # 數字總和 + 檢查輸入

    if check_num % 10 == 0:
        print('Valid ID number')
    else:
        print('Invalid ID number')
    # 輸出驗證


def IDvalidation(s):
    checkLength(s)
    changed = changeID(s)
    checkID(changed)
# 主函式庫


try:
    IDvalidation(input('please input an ID : '))

except EOFError as error:
    print(error)

os.system('pause')
# 主程式
