import os
import timeit
import math


def pascal_nonrecursive(row, col):  # non recursive algorithm (Maybe...)
    if col == 0 or col == row:
        return 1
    else:
        return (pascal_nonrecursive(row - 1, col - 1) + pascal_nonrecursive(row - 1, col))


def nCr(n, r):  # combination algorithm
    f = math.factorial
    return f(n) / f(r) / f(n - r)


def PA_r(hierarchy):  # PA_r function (array add together)
    if hierarchy == 1:
        print([1])
        return [1]
    else:
        a = b = PA_r(hierarchy - 1)
        a = a + [0]
        b = [0] + b
        c = [int(a[i]) + int(b[i]) for i in range(hierarchy)]
        print(c)
        return c


def PA_n(hierarchy):  # PA_n function
    array = [0]
    for j in range(hierarchy):  # 層數
        for i in range(0, j + 1):   # 位置
            array[i] = pascal_nonrecursive(j, i)
        print(array)
        array = array + [0]


def PA_d(hierarchy):  # PA_d function
    array = [0]
    for j in range(hierarchy):  # 層數
        for i in range(0, j + 1):
            array[i] = int(nCr(j, i))
        print(array)
        array = array + [0]


try:  # Main function
    i = input('input the degree n : ')
    if ((float(i) * 1000) % 1000) != 0:
        raise EOFError('\tn degree can only be integer !')

    n = int(i)
    PA_n(n)
    print('Recursion execution time : \t\t', timeit.timeit(
        stmt='from __main__ import PA_r, n', number=1))
    print('Non-Recursion execution time : \t\t', timeit.timeit(
        stmt='from __main__ import PA_n, n', number=1))
    print('Direct Computation execution time : \t', timeit.timeit(
        stmt='from __main__ import PA_d, n', number=1))

    os.system('pause')

except EOFError as error:
    print(error)
