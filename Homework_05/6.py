import math

Node = 0


class Heap:
    # initial value priority and parent
    def __init__(self, _item=None, _priority=None, _parent=None):
        self.item = _item
        self.left = None
        self.right = None
        self.priority = _priority
        self.parent = _parent

    def __str__(self):  # Use for print node
        return str(self.item)

    def Insert(self, value):    # Insert node by string argument
        global Node
        data = value.split()    # split by space
        try:  # check input valid or not
            _data1 = isinstance(int(data[0]), int)
            _data2 = isinstance(data[1], str)
        except:
            raise EOFError('Input argument error !')
        if _data1 is False or _data2 is False or len(data) is not 2:
            raise EOFError('Input argument error !')

        if self.item is None:   # Root Node
            Node += 1
            self.priority = data[0]
            self.item = data[1]
        elif self.left is None:  # add by left
            Node += 1
            temp = Heap(data[1], data[0], self)
            self.left = temp
            self.upwardHeapify(self.left)
        elif self.right is None:    # add by right
            Node += 1
            temp = Heap(data[1], data[0], self)
            self.right = temp
            self.upwardHeapify(self.right)
        elif self.right.right is None:  # if layer is full
            if self.right.left is None:
                self.left.Insert(value)
            else:
                self.right.Insert(value)

    def key(self):  # return key value
        return self.priority

    def swap(self, v):  # use for swap node value and priority
        self.item, v.item = v.item, self.item
        self.priority, v.priority = v.priority, self.priority

    def downwardHeapify(self, v):   # Downward Heapify to min Heap
        left = v.left.key() if v.left is not None else float('inf')
        right = v.right.key() if v.right is not None else float('inf')
        if float(left) >= float(right):
            if float(v.key()) > float(right):
                v.swap(v.right)
        else:
            if float(v.key()) > float(left):
                v.swap(v.left)

    def upwardHeapify(self, v):  # Upward Heapify to min Heap
        p = v.parent.key() if v.parent is not None else float('-inf')
        if float(v.key()) < float(p):
            v.swap(v.parent)

    def last(self):  # Find the last node
        global Node
        Q = [Node]
        R = []
        if Node is not 0:
            while Q[-1] is not 1:
                R.append(int(Q[-1] % 2))
                Q[-1] -= R[-1]
                Q.append(int(Q[-1] / 2))
        _Node = self
        for i in R:
            _Node = _Node.left if i is 0 else _Node.right
        return _Node

    def removeMin(self):    # Remove the root Node by swap with the last element
        global Node
        if Node > 1:    # More than one Node
            Temp_Node = self.last()
            self.swap(Temp_Node)
            if Temp_Node.parent.left is not None and Temp_Node.parent.left.item is Temp_Node.item:
                Temp_Node.parent.left = None
            else:
                Temp_Node.parent.right = None
            Node -= 1
            print('remove :', Temp_Node.priority, Temp_Node.item)
            del Temp_Node
            self.downwardHeapify(self)
        elif Node == 1:  # Only root Node
            print('remove :', self.priority, self.item)
            self.item = self.priority = self.parent = None
            Node -= 1
            print('The heap is now empty')
        else:   # No node in this heap
            print('The heap is empty and no entry can be removed')

    def printHeapPreOrder(self, v):  # Find the pre-order by Euler method
        print('Leaf [', v.priority, v.item, ']') if v.left is None and v.right is None else print(
            'Node [', v.priority, v.item, ']')
        if v.left is not None:
            self.printHeapPreOrder(v.left)
        if v.right is not None:
            self.printHeapPreOrder(v.right)


def HeapwithEntriesInserted():  # Main program
    global Node
    try:
        f = open('inFile.txt', 'r')  # Open file
        line = f.readline()  # read first line
        main = Heap()
        while line:  # read every line
            main.Insert(line)
            line = f.readline()
        f.close()  # close file

        # >--------------------------Test File--------------------------------<
        print('Heap Size =', Node, 'The highest priority is :', main.priority)
        print('pre-order traversal:')
        main.printHeapPreOrder(main)
        main.removeMin()  # remove 10 mary
        main.removeMin()  # remove 25 john
        main.removeMin()  # remove 35 mars
        main.removeMin()  # remove 50 lowe (empty)
        main.removeMin()  # none item can be removed
        print('insert 35, resume')
        main.Insert('35 resume')
        print('insert 15, second')
        main.Insert('15 second')
        print('insert 20, fourth')
        main.Insert('20 fourth')
        print('Heap Size =', Node, 'The highest priority is :', main.priority)
        main.printHeapPreOrder(main)
        # >--------------------------Test File--------------------------------<

    except (EOFError, ValueError, EnvironmentError) as error:
        print(error)


HeapwithEntriesInserted()
