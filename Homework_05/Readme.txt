班級 : 電資二
學號 : 106820007
姓名 : 段凱文

---------------------------作業五------------------------------------
執行 :	直接雙點擊 6.py 即可 	(預設讀取 inFile.txt 檔案)

				可以使用的 function :

				removeMin()						會輸出移除的節點

				Insert(v) 						v 需要以 str 的形式加入，
															前為數值，後為資料

				upwardHeapify(v) 			需要以root.upwardHeapify(v)
															來呼叫，並且以 v 為結點向上排
															序 v 的資料型態必為 Heap

				downwardHeapify(v)		同上面，以向下排序之

				key()									以 node.key() 呼叫，回傳
															priority

				last()								以 root.last() 呼叫，可以找到
															最後一個節點

				printHeapPreOrder(v)	以 node.printHeapPreOrder(v) 呼叫
															從 v 的節點來搜尋，以 Euler Tour
															尋找


發現 :	以 Link list 來建立 Heap ，並且加入基本的 Heap 加入 &刪除 ，
				額外使用 Euler Tour 來找 pre-order 的形式
