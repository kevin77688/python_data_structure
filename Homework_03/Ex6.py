import os
import math


def reverse_string(s):  # 把字串顛倒輸出 Ex: Apple -> elppA
    return s[::-1]


class Stack:  # 以陣列的方式來模擬堆疊
    def __init__(self):  # 初始化
        self.items = []

    def isEmpty(self):  # 檢測是否空堆疊
        return self.items == []

    def push(self, item):  # 將物品增加近Stack
        self.items.append(item)

    def pop(self):  # 丟出物品
        return self.items.pop()

    def peek(self):  # 查看最上層的物品 (不會被丟棄)
        return self.items[len(self.items) - 1]

    def peek(self, number):  # (Overloaded) 查看上面第N個物品
        return self.items[len(self.items) - 1 - number]

    def size(self):  # 堆疊大小 = 陣列大小
        return len(self.items)

    def switch(self, index, item):  # 把堆疊第index個東西改為item
        temp = Stack()
        while(index != 0):
            index -= 1
            temp.push(self.pop())
        self.pop()
        self.push(item)
        while temp.isEmpty() is not True:
            self.push(temp.pop())

    def output(self):  # 將堆疊全部顯示，並且顛倒
        s = ''
        for i in range(self.size()):
            s += str(self.peek(i))
        return reverse_string(s)  # 因為LIFO的特性，所以第一位數會變成100，要顛倒成001


def change_digit(n):  # 1 -> 0  ;  0 -> 1
    if n == 1:
        return 0
    elif n == 0:
        return 1
    else:
        raise EOFError('Transform Digit Error')


def check_not_in_array(arr, item):  # 檢測物件是否在陣列中
    for i in range(len(arr)):
        if arr[i] == item:
            return False
    return True


def check_num(str):  # 檢測輸入並轉換成int
    try:
        val = int(str)
        if val <= 0:
            raise EOFError('Input digit can be only bigger than 0')
        return val
    except ValueError:
        raise EOFError('Input error, you can only input interger !')


def generate_array(num):  # 建立'000'的陣列 (共2^n次個)
    num = int(num)
    s = '0'
    for k in range(num - 1):
        s += '0'
    return [s for i in range(int(math.pow(2, num)))]


def transform_to_gray(arr, num):  # 將陣列與維度轉換成Gray Code
    num = int(num)
    gray = Stack()  # 初始化Stack
    for _ in range(num):
        gray.push(0)  # 建立n個0
    for i in range(1, int(math.pow(2, num))):
        for j in range(num):
            gray.switch(j, change_digit(gray.peek(j)))  # 把第 j 的數值轉換 1 & 0
            if (check_not_in_array(arr, gray.output())):  # 鑑測是否存在於陣列
                arr[i] = gray.output()
                break
            else:
                # 若是存在於陣列，回到上一步，並跳至下一個位數
                gray.switch(j, change_digit(gray.peek(j)))
                continue
    return arr


def Gray_Code():
    i = input('Please enter the number of bits for the Gray code :')
    array = generate_array(check_num(i))
    print(transform_to_gray(array, i))


# 主程式
try:    # Main function
    Gray_Code()
except EOFError as error:
    print(error)

os.system('pause')
