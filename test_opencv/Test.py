import os

import cv2
import numpy as np
import pygame
from pygame.locals import *

if not hasattr(pygame, "Mask"):
    raise "Need pygame 1.8 for masks."

WINDOW_WIDTH = 707
WINDOW_HEIGHT = 354
CAPTURE = False

lower_blue = np.array([110, 50, 50], dtype=np.uint8)
upper_blue = np.array([130, 255, 255], dtype=np.uint8)
cap = cv2.VideoCapture(0)

pygame.display.init()
pygame.font.init()

pygame.display.set_caption("Balloon!  Don't hit the walls")


screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.key.set_repeat(500, 2)
clock = pygame.time.Clock()


# fill the screen with red... for dramatic effect.
screen.fill((255, 0, 0))
pygame.display.flip()


def load_image(i):
    return pygame.image.load(os.path.join("data", i)).convert_alpha()


terrain1 = load_image("terrain1.png")
balloon = load_image("balloon.png")

# create a mask for each of them.
terrain1_mask = pygame.mask.from_surface(terrain1, 50)
balloon_mask = pygame.mask.from_surface(balloon, 50)

# this is where the balloon, and terrain are.
terrain1_rect = terrain1.get_rect()
balloon_rect = balloon.get_rect()


# a message for if the balloon hits the terrain.
afont = pygame.font.Font(None, 16)
hitsurf = afont.render("Hit!!!  Oh noes!!", 1, (255, 255, 255))


last_bx, last_by = 0, 0


def find_blue():
    ret, frame = cap.read()
    if frame is not None:
        CAPTURE = True
        frame = cv2.flip(frame, 1)
        frame = cv2.resize(frame, (WINDOW_WIDTH, WINDOW_HEIGHT))
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
        maskBlue = cv2.erode(maskBlue, kernel, iterations=3)
        maskBlue = cv2.dilate(maskBlue, kernel, iterations=3)
        cv2.imshow('maskBlue', maskBlue)
        cv2.imshow('frame', hsv)
        centerBlue = None
        cnts = cv2.findContours(
            maskBlue.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ((xBlue, yBlue), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            cx = int(M["m10"] / M["m00"])
            cy = int(M["m01"] / M["m00"])
            centerBlue = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            if radius > 10:
                mode = False
                cv2.circle(frame, centerBlue, 5, (0, 0, 255), -1)
        else:
            CAPTURE = False
            cx = 30
            cy = 300

        return [cx, cy, CAPTURE]
        # balloon_rect[0] = cx
        # balloon_rect[1] = cy


# start the main loop.
while True:
    center = find_blue()
    events = pygame.event.get()
    _cap = center[2]

    # see how far the balloon rect is offset from the terrain rect.
    if _cap is False:
        print(abs(last_bx - center[0]), abs(last_by - center[1]))
        bx, by = (0, 0)
        balloon_rect[0], balloon_rect[1] = (0, 0)
        if abs(last_bx - center[0]) < 50 and abs(last_by - center[1]) < 50:
            _cap = True
    if _cap is True:
        balloon_rect[0], balloon_rect[1] = center[0], center[1]
        bx, by = (center[0], center[1])
    # else:
    #     bx, by = (0, 0)
    offset_x = bx - terrain1_rect[0]
    offset_y = by - terrain1_rect[1]

    # print bx, by
    overlap = terrain1_mask.overlap(balloon_mask, (offset_x, offset_y))

    #
    last_bx, last_by = bx, by

    # draw the background color, and the terrain.
    screen.fill((255, 0, 0))
    screen.blit(terrain1, (0, 0))

    # draw the balloon.
    screen.blit(balloon, (balloon_rect[0], balloon_rect[1]))

    # draw the balloon rect, so you can see where the bounding rect would be.
    pygame.draw.rect(screen, (0, 255, 0), balloon_rect, 1)

    if _cap is False:
        pygame.draw.circle(screen, (66, 244, 235), center[0:2], 8)

    # see if there was an overlap of pixels between the balloon
    #   and the terrain.
    if overlap:
        # we have hit the wall!!!  oh noes!
        screen.blit(hitsurf, (0, 0))

    # flip the display.
    pygame.display.flip()

    # limit the frame rate to 40fps.
    clock.tick(40)


pygame.quit()
