import random
import time

import cv2
import numpy as np
import pygame

HIT = 0

WINDOW_WIDTH = 1200
WINDOW_HEIGHT = 600
PADDLE_START_X = (WINDOW_WIDTH / 2)
PADDLE_START_Y = (WINDOW_HEIGHT / 2)
PADDLE_WIDTH = 800
PADDLE_HEIGHT = 8
PADDLE_COLOR = (238, 130, 238)
windowSurface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
paddle = {'rect': pygame.Rect(0, 0, PADDLE_WIDTH, PADDLE_HEIGHT),
          'color': PADDLE_COLOR}
PADDLE_COLOR = (238, 130, 238)
pygame.init()

lower_blue = np.array([110, 50, 50], dtype=np.uint8)
upper_blue = np.array([130, 255, 255], dtype=np.uint8)
cap = cv2.VideoCapture(0)


gameDisplay = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('Space')
clock = pygame.time.Clock()

d = pygame.image.load('d.jpg')
a = pygame.image.load('a.png')
c = pygame.image.load('c.png')


# class Circle:
#     def __init__(self, _cx, _cy):
#         self.cx = _cx
#         self.cy = _cy
#         self.image = pygame.Surface((cx, cy), pygame.SRCALPHA)
#         # self.image.fill(0, 0, 0)
#         self.rect = self.image.get_rect(center=(cx, cy))
#         self.mask = pygame.mask.from_surface(self.image)
#
#     def draw(self):
#         pygame.draw.circle(self.image, (0, 255, 0), (cx, cy), 20, 3)


# def bDisplay(x, y):
#     Circle(x, y).draw()
# pygame.draw.circle(windowSurface, (0, 255, 0), (cx, cy), 20, 3)
# c.draw()
# pygame.draw.circle(windowSurface, (0, 255, 0), (cx, cy),
#                    20, 3)


gameExit = False

while not gameExit:
    paddle['rect'].left = PADDLE_START_X
    paddle['rect'].top = PADDLE_START_Y

    ret, frame = cap.read()
    frame = cv2.flip(frame, 1)
    frame = cv2.resize(frame, (WINDOW_WIDTH, WINDOW_HEIGHT))
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    maskBlue = cv2.erode(maskBlue, kernel, iterations=3)
    maskBlue = cv2.dilate(maskBlue, kernel, iterations=3)
    cv2.imshow('maskBlue', maskBlue)
    cv2.imshow('frame', hsv)
    pygame.draw.rect(windowSurface, paddle['color'], paddle['rect'])
    centerBlue = None
    cnts = cv2.findContours(
        maskBlue.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)
        ((xBlue, yBlue), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        cx = int(M["m10"] / M["m00"])
        cy = int(M["m01"] / M["m00"])
        centerBlue = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        if radius > 10:
            mode = False
            cv2.circle(frame, centerBlue, 5, (0, 0, 255), -1)
    else:
        cx = 0
        cy = 0

    circle_surface = pygame.Surface((6, 6), pygame.SRCALPHA)
    pygame.draw.circle(circle_surface, (0, 255, 0), (cx, cy), 3)

    background_img = a.convert_alpha()
    background_mask = pygame.mask.from_surface(background_img, 50)
    background_rect = background_img.get_rect()

    # Cir = Circle(cx, cy)
    # offset_x = Cir.rect[0] - background_rect[0]
    # offset_y = Cir.rect[1] - background_rect[1]
    # if background_mask.overlap(Cir.mask, (offset_x, offset_y)):
    #     HIT += 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
    # gameDisplay.blit(d, (0, 0))
    # gameDisplay.blit(a, (0, 0))
    # bDisplay(cx, cy)
    pygame.draw.circle(circle_surface, (0, 255, 0), (cx, cy), 3)
    pygame.display.update()
    # gameDisplay.fill((0, 0, 0))
    # gameDisplay.blit(d, (0, 0))
    # gameDisplay.blit(a, (0, 0))
    # print(HIT)
    clock.tick(60)
pygame.quit()
quit()
