import os

import cv2
import numpy as np
import pygame as pg

WINDOW_WIDTH = 1200
WINDOW_HEIGHT = 600
windowSurface = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
GREEN = (0, 255, 0)
pg.init()

lower_blue = np.array([110, 50, 50], dtype=np.uint8)
upper_blue = np.array([130, 255, 255], dtype=np.uint8)
cap = cv2.VideoCapture(0)

gameDisplay = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pg.display.set_caption('Space')
clock = pg.time.Clock()

d = pg.image.load('d.jpg')
a = pg.image.load(os.path.join("terrain1.png")).convert_alpha()

bg = a
circle_surface = pg.Surface((40, 40), pg.SRCALPHA)

bg_mask = pg.mask.from_surface(bg, 50)
circle_mask = pg.mask.from_surface(circle_surface, 50)

bg_rect = bg.get_rect()
circle_rect = circle_surface.get_rect()
# c = pg.image.load('c.png')


def find_blue():
    ret, frame = cap.read()
    frame = cv2.flip(frame, 1)
    frame = cv2.resize(frame, (WINDOW_WIDTH, WINDOW_HEIGHT))
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    maskBlue = cv2.erode(maskBlue, kernel, iterations=3)
    maskBlue = cv2.dilate(maskBlue, kernel, iterations=3)
    cv2.imshow('maskBlue', maskBlue)
    cv2.imshow('frame', hsv)
    centerBlue = None
    cnts = cv2.findContours(
        maskBlue.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)
        ((xBlue, yBlue), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        cx = int(M["m10"] / M["m00"])
        cy = int(M["m01"] / M["m00"])
        centerBlue = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        if radius > 10:
            mode = False
            cv2.circle(frame, centerBlue, 5, (0, 0, 255), -1)
    else:
        cx = 0
        cy = 0
    center = [cx, cy]
    return center


def display(center):
    gameDisplay.blit(d, (0, 0))
    gameDisplay.blit(a, (0, 0))
    pg.draw.circle(windowSurface, GREEN, center, 20, 3)
    pg.display.flip()
    gameDisplay.fill((0, 0, 0))
    gameDisplay.blit(d, (0, 0))
    gameDisplay.blit(a, (0, 0))


while True:
    center = find_blue()
    for event in pg.event.get():
        if event.type == pg.QUIT:
            pg.quit()
            quit()
    # circle_surface = pg.Surface((40, 40), pg.SRCALPHA)
    # pg.draw.circle(circle_surface, GREEN, center, 20, 3)
    # circle_mask = pg.mask.from_surface(circle_surface, 50)
    # circle_rect = circle_surface.get_rect(center=center)
    #
    # bg_surface = a
    # bg_mask = pg.mask.from_surface(a, 50)
    # bg_rect = a.get_rect()

    offset_x = circle_rect[0] - bg_rect[0]
    offset_y = circle_rect[1] - bg_rect[1]
    overlap = bg_mask.overlap(circle_mask, (offset_x, offset_y))
    print(overlap)
    # print(bg_rect)
    if overlap:
        print('The two masks overlap!', overlap)

    display(center)
    clock.tick(60)
pg.quit()
quit()
